class RangeList

  def initialize
    @range_list = []
  end

  def add(range)
    range_start = range.first
    range_end = range.last

    # 如果开始大于等于结束，不做任何处理
    return if range_start >= range_end

    # 初始起点位置
    start_index = 0

    new_range_list = []

    # 遍历找到插入的起始位置
    for i in 0..@range_list.size - 1
      current_range, current_range_start, current_range_end =
        current_range_start_end(i)

      if range_start > current_range_end

        new_range_list << current_range
        start_index = i + 1

      elsif range_start > current_range_start

        range_start = current_range_start
        break

      end
    end
    # start_index为起始位置
    # 遍历插入的结束位置
    end_index = 0
    for i in 0..@range_list.size - 1
      current_range, current_range_start, current_range_end =
        current_range_start_end(i)

      if range_end > current_range_end
        if i > start_index && i != @range_list.size - 1
          new_range_list << current_range
        end
        end_index = i + 1
      elsif range_end >= current_range_start
        range_end = current_range_end
        end_index = i + 1
        break
      end
    end
    # 将找到的范围的开始和结束推到后面以将其添加到范围列表中
    new_range_list << [range_start, range_end]
p 11111
p new_range_list
p @range_list
    # 连接左边
    @range_list = new_range_list + @range_list[end_index, @range_list.size]
  end

  def remove(range)
    range_start = range.first
    range_end = range.last

    # 如果开始大于等于结束，不做任何处理
    return if range_start >= range_end

    new_range_list = []

    # 遍历找到插入的起始位置
    for i in 0..@range_list.size - 1
      current_range, current_range_start, current_range_end =
        current_range_start_end(i)

      if range_start > current_range_end

        new_range_list << current_range

      elsif range_start > current_range_start

        new_range_list << [current_range_start, range_start]

        break
      end
    end

    # start_index为起始位置
    # 遍历插入的结束位置
    end_index = 0
    for i in 0..@range_list.size - 1
      current_range, current_range_start, current_range_end =
        current_range_start_end(i)

      if range_end < current_range_end
        if range_end >= current_range_start
          if range_end != current_range_end
            new_range_list << [range_end, current_range_end]
          end
        else
          new_range_list << current_range
        end
        end_index += 1
        break
      else
        end_index += 1
      end
    end

    @range_list = new_range_list + @range_list[end_index, @range_list.size]
  end

  def print
    @range_list.each do |range|
      p "[#{range.first}, #{range.last})"
    end
  end

  private

  def current_range_start_end(i)
    current_range = @range_list[i]
    current_range_start = current_range.first
    current_range_end = current_range.last
    [current_range, current_range_start, current_range_end]
  end
end